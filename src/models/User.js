const { model, Schema } = require("mongoose");
const bcrypt = require('bcrypt');

const UserSchema = new Schema({
  cpf: {
    type: String,
    unique: true,
  },
  password: String,
  userType: { 
    type: String,
    enum: ['admin', 'client', 'attendant'],
    default: 'client'
  },
  isActive: { 
    type: Boolean,
    default: true
  },
  created: { type: Date, default: Date.now }
});


UserSchema.pre('save', async function (next){
  const user = this;
  if(user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

UserSchema.statics.findByCredentials = async (cpf, password) => {
  try {
    const user = await User.findOne( {cpf});

    if(!user)
      throw new Error('Usuário não encontrado!');

    const isMatch = await bcrypt.compare(password, user.password);
    if(!isMatch)
      throw new Error('A sua senha está incorreta!');

    return user;
  }catch(err) {
    throw new Error(err.message);
  }
}

const User = model('User', UserSchema);

module.exports = User;
