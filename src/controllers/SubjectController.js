const Subject = require("../models/Subject");

module.exports = {
    async index(request, response) {
        const subjects = await Subject.find();

        return response.json(subjects);
    },

    async store(request, response) {
        const { name } = request.body;

        let subject;

        if (name) {
            subject = await Subject.create({ name });
        } else {
            return response.json({"mensagem": "Faltando parâmetros."});
        }

        return response.json(subject);
    }

};