const User = require("../models/User");
const Client = require("../models/Client");
const Attendant = require("../models/Attendant");

module.exports = {
    async index(request, response) {
        const users = await User.find();

        return response.json(users);
    },

    async store(request, response) {
        const { cpf, password, userType = 'client' } = request.body;

        let user;

        if (cpf && password) {
            user = await User.create({ cpf, password, userType });
        } else {
            return response.json({"mensagem": "Faltando parâmetros."});
        }

        return response.json(user);
    },

    async auth(request, response) {
        const { cpf, password } = request.body;
        try {
            const user = await User.findByCredentials(cpf, password);
            if(user.userType === 'client') {
                const client = await Client.findOne({user: user._id});

                return response.status(200).json(
                    {
                        client: client, 
                        cpf: user.cpf, 
                        userId: user._id, 
                        userType: user.userType
                    }
                );
            } else if(user.userType === 'attendant') {
                const attendant = await Attendant.findOne({user: user._id});

                return response.status(200).json(
                    {
                        attendant: attendant, 
                        cpf: user.cpf, 
                        userId: user._id, 
                        userType: user.userType
                    }
                );
            }
        }catch(err) {
           return response.status(500).json({
                "sucesso": false,
                "mensagem": err.message
            });
        }
    }

};