const Client = require("../models/Client");
const User = require("../models/User");

module.exports = {

    async store(request, response) {
        const { cpf, password, name, email, phone, state } = request.body;

        if (cpf && password && name && email && phone && state) {

            const user = await User.create({cpf, password});
            await Client.create({ user, name, email, phone, state });
        } else {
            return response.json({"mensagem": "Faltando parâmetros."});
        }

        return response.status(200);
    }

};