const Ticket = require("../models/Ticket");

module.exports = {
    async index(request, response) {
        const tickets = await Ticket.find().populate('client').populate('subject');

        return response.json(tickets);
    },

    async store(request, response) {
        const { client, subject } = request.body;

        let ticket;

        if (client && subject) {
            ticket = await Ticket.create({ client, subject });
        } else {
            return response.json({"mensagem": "Faltando parâmetros."});
        }

        return response.json(ticket);
    },

    async show(request, response) {
        const id = request.params.id;

        const ticket = await Ticket.findOne({_id: id});

        return response.json(ticket);
    },

    async update(request, response) {
        const id = request.body;

        const ticket = await Ticket.findOne({user: user._id});

        ticket.save({ status: 'finalizado' });

        return response.json(ticket);
    },

};