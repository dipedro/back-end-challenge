const { model, Schema } = require("mongoose");

const ConversationSchema = new Schema({
  ticket: { 
    type: Schema.Types.ObjectId,
    ref: 'Ticket',
    required: true
  },
  user:  { 
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  message: String,
  created: { type: Date, default: Date.now }
});

module.exports = model("Conversation", ConversationSchema);
