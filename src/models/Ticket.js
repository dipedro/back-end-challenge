const { model, Schema } = require("mongoose");

const TicketSchema = new Schema({
  client: { 
    type: Schema.Types.ObjectId,
    ref: 'Client',
    required: true
  },
  subject:  { 
    type: Schema.Types.ObjectId,
    ref: 'Subject',
    required: true
  },
  status:  { 
    type: String,
    enum: ['pendente', 'iniciado', 'finalizado'],
    default: 'pendente'
  },
  created: { type: Date, default: Date.now }
});

module.exports = model("Ticket", TicketSchema);
