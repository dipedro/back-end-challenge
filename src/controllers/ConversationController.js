const Conversation = require("../models/Conversation");

module.exports = {

    async store(request, response) {
        const { ticket, user, message } = request.body;

        let conversation;

        if (ticket && user && message) {

            conversation = await Conversation.create({
                ticket, user, message
            });
        } else {
            return response.json({"mensagem": "Faltando parâmetros."});
        }

        return response.json(conversation);
    },

    async show(request, response) {
        const ticket_id = request.params.id;

        const conversations = await Conversation.find().where('ticket', ticket_id);

        return response.json(conversations);
    }

};