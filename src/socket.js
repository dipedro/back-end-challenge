const Conversation = require("./models/Conversation");

const socket = io => {

    let users = {};

    io.on('connection', client => {
        console.log('Cliente conectado: ' + client.id);

        client.on('registerClient', async (data) => {
            users[data.client_id] = data;

            const messages = await Conversation.find({ticket: data.ticket_id});

            client.emit('historyMessages', messages);
        });

        client.on('sendMessage', async (data) => {

            const conversation = await Conversation.create(
                {
                    user: data.userId, 
                    ticket: data.ticket , 
                    message: data.message
                });
            if(conversation)
                client.emit('receivedMessage', data);
        });
    });
}

module.exports = socket;