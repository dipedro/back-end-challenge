const { model, Schema } = require("mongoose");

const AttendantSchema = new Schema({
  user: { 
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  name: String,
  email: String,
  phone: String,
  created: { type: Date, default: Date.now }
});

module.exports = model("Attendant", AttendantSchema);
