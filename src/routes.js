const { Router } = require('express');
const AttendantController = require('./controllers/AttendantController');
const ClientController = require('./controllers/ClientController');
const ConversationController = require('./controllers/ConversationController');
const FaqController = require('./controllers/FaqController');
const SubjectController = require('./controllers/SubjectController');
const TicketController = require('./controllers/TicketController');
const UserController = require('./controllers/UserController');

const routes = Router();

routes.get('/', (request, response) => {
    return response.json({ mensagem: 'Olá mundo!'});
});

routes.post('/auth', UserController.auth);

routes.post('/clients', ClientController.store);

routes.post('/attendants', AttendantController.store);

routes.get('/faqs', FaqController.index);
routes.post('/faqs', FaqController.store);

routes.get('/subjects', SubjectController.index);
routes.post('/subjects', SubjectController.store);

routes.get('/tickets', TicketController.index);
routes.get('/tickets/:id', TicketController.show);
routes.post('/tickets', TicketController.store);
routes.put('/tickets', TicketController.update);

routes.get('/conversations/:id', ConversationController.show);
routes.post('/conversations', ConversationController.store);

routes.get('/users', UserController.index);
routes.post('/users', UserController.store);

module.exports = routes;