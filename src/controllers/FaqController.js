const Faq = require("../models/Faq");

module.exports = {
    async index(request, response) {
        const faqs = await Faq.find();

        return response.json(faqs);
    },

    async store(request, response) {
        const { question, answer } = request.body;

        let faq;

        if (question && answer) {
            faq = await Faq.create({ question, answer });
        } else {
            return response.json({"mensagem": "Faltando parâmetros."});
        }

        return response.json(faq);
    }

};