const Attendant = require("../models/Attendant");
const User = require("../models/User");

module.exports = {

    async store(request, response) {
        const { cpf, password, name, email, phone, userType = 'attendant' } = request.body;
        try {
            if (cpf && password && name && email && phone) {
                const user = await User.create({cpf, password, userType});
                await Attendant.create({ user, name, email, phone });
            } else {
                return response.status(500).json({"mensagem": "Faltando parâmetros."});
            }
    
            return response.status(200);
        }catch(err) {
            return response.send(err);
        }
        
    }

};