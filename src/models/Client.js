const { model, Schema } = require("mongoose");

const ClientSchema = new Schema({
  user: { 
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  name: String,
  email: String,
  phone: String,
  state: String,
  created: { type: Date, default: Date.now }
});

module.exports = model("Client", ClientSchema);
