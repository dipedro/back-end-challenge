# Backend

Backend da aplicação SAC Mateus.

### Tecnologias
NodeJS, MongoDB (utilizei o banco grátis do MongoDBAtlas) e Socket.io

## Instalação

Clone o repositório, entre na pasta do projeto clonado e digite o seguinte comando:

```bash
npm install
```

## Execução

Digite o seguinte comando caso esteja utilizando o npm.

```bash
~ npm start

# Aguarde até aparecer a seguinte mensagem:
~ Server is running on port 3001.
```

## Rotas

```javascript
routes.get('/');

routes.post('/auth');

routes.post('/clients');

routes.post('/attendants');

routes.get('/faqs');
routes.post('/faqs');

routes.get('/subjects');
routes.post('/subjects');

routes.get('/tickets');
routes.get('/tickets/:id');
routes.post('/tickets');
routes.put('/tickets');

routes.get('/conversations/:id');
routes.post('/conversations');

routes.get('/users');
routes.post('/users');

```

## License
[MIT](https://choosealicense.com/licenses/mit/)