const express = require('express');
const routes = require('./routes');
const cors = require('cors')
require('./utils/db');

const app = express();

app.use(cors());
app.use(express.json());

app.use(routes);

const port = 3001;

const server = app.listen(port, () => {
  console.log(`\nServer is running on port ${port}.`);
});

var io = require('socket.io').listen(server);
require('./socket')(io);
