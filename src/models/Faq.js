const { model, Schema } = require("mongoose");

const FaqSchema = new Schema({
  question: String,
  answer: String,
  created: { type: Date, default: Date.now }
});

module.exports = model("Faq", FaqSchema);
