const { model, Schema } = require("mongoose");

const SubjectSchema = new Schema({
  name: String,
  created: { type: Date, default: Date.now }
});

module.exports = model("Subject", SubjectSchema);
